package com.losgd.los.views;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

public class AnimationPacker {
	public static HashMap<String, Array<AnimationConstructor>> animations;

	private Array<TextureRegion> buffer;

	private Array<AnimationConstructor> build;

	public AnimationPacker() {
		buffer = new Array<TextureRegion>();
		animations = new HashMap<String, Array<AnimationConstructor>>();
		build = new Array<AnimationConstructor>();
	}

	public void LoadAnimations(String atlasPath, String configPath) {
		TextureAtlas atlas = new TextureAtlas(atlasPath);
		JsonReader json = new JsonReader();
		JsonValue value = json.parse(Gdx.app.getFiles().local(configPath));
		value = value.get("AnimationConstruct");

		for (JsonValue AnimationConstruct : value) {
			String animName = AnimationConstruct.getString("name");

			for (JsonValue Animation : AnimationConstruct.get("Animation")) {

				for (JsonValue Frame : Animation.get("Frame")) {

					buffer.add(atlas.findRegion(Frame.toString()));
				}
				build.add(new AnimationConstructor(new Animation(Animation.getFloat("Duration"), buffer),
						Animation.getFloat("x"), Animation.getFloat("y"), Animation.getFloat("width"),
						Animation.getFloat("height"), AnimationConstruct.getFloat("CenterX"),
						AnimationConstruct.getFloat("CenterY")));
				buffer.clear();
			}
			animations.put(animName, new Array<AnimationConstructor>(build));
			build.clear();
		}
	}
}
