package com.losgd.los.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.losgd.los.models.Plane;

public class GameRenderer {

	private SpriteBatch batch;
	OrthographicCamera camera = null;
	private TextureAtlas atlas;
	private Plane player;

	public static float maintime = 0;

	public GameRenderer() {

		AnimationPacker loader = new AnimationPacker();
		loader.LoadAnimations("Textures/Los.atlas", "Textures/Animations.json");

		batch = new SpriteBatch();
		initAnimations();
	};

	public void LoadPlayer(Plane player) {
		this.player = player;
	}

	public void getCamera(OrthographicCamera camera) {
		this.camera = camera;
	}

	public AtlasRegion getRegion(int id) {
		return atlas.getRegions().get(id);
	}

	public void render() {
		maintime += Gdx.graphics.getDeltaTime();
		if (camera == null) {
			System.out.println("Error, Render camera is not loaded");
			return;
		}
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		player.render(batch);
		batch.end();
	}

	private void initAnimations() {

	}

}
