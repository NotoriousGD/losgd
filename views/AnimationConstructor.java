package com.losgd.los.views;

import static com.losgd.los.Vars.scale;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class AnimationConstructor {
	Animation animation;
	private float offx, offy;
	private float width, height;
	Vector2 center;

	public AnimationConstructor(Animation animation, float offx, float offy, float width, float height, float CenterX,
			float CenterY) {
		this.animation = animation;
		this.offx = offx;
		this.offy = offy;
		this.width = width;
		this.height = height;
		center = new Vector2(CenterX, CenterY);
	}

	public void render(Body body, SpriteBatch batch) {
		batch.draw(animation.getKeyFrame(GameRenderer.maintime, true),
				body.getPosition().x - scale(center.x) + scale(offx),
				body.getPosition().y - scale(center.y) + scale(offy), scale(center.x) - scale(offx),
				scale(center.y) - scale(offy), scale(width), scale(height), 1, 1,
				body.getAngle() * MathUtils.radiansToDegrees);
	}
}
