package com.losgd.los.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.losgd.los.controllers.Controller;
import com.losgd.los.models.GameWorld;

public class GameScreen implements Screen {

	Box2DDebugRenderer box2DDebugRenderer;

	GameWorld gameWorld;

	Controller controller;

	@Override
	public void show() {
		box2DDebugRenderer = new Box2DDebugRenderer(true, true, true, true, true, true);
		gameWorld = new GameWorld();
		controller = new Controller(gameWorld.getPlayer(), gameWorld.getOrthographicCamera());
		Gdx.input.setInputProcessor(controller);
	}

	@Override
	public void render(float delta) {
		controller.step(delta);
		gameWorld.step();
		box2DDebugRenderer.render(gameWorld.getWorld(), gameWorld.getOrthographicCamera().combined);
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}
}
