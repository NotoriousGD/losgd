package com.losgd.los.screens;

import com.badlogic.gdx.Screen;
import com.losgd.los.managers.CustomScreen;
import com.losgd.los.managers.ScreenManager;

public class MenuScreen implements Screen{
    @Override
    public void show() {
        ScreenManager.getInstance().show(CustomScreen.GAME_SCREEN);
    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
