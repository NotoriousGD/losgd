package com.losgd.los.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.losgd.los.managers.CustomScreen;
import com.losgd.los.managers.GameAssetManager;
import com.losgd.los.managers.ScreenManager;
import com.losgd.los.views.AnimationPacker;

public class LoadScreen implements Screen {

	AnimationPacker animpack;
    private void loadAssets(){
        GameAssetManager.getInstance().load("badlogic.jpg",Texture.class);
    }

    @Override
    public void show() {
        loadAssets();
        animpack = new AnimationPacker();
    }

    @Override
    public void render(float delta) {
        if(GameAssetManager.getInstance().update()){
            ScreenManager.getInstance().show(CustomScreen.MENU_SCREEN);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
