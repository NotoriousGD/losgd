package com.losgd.los;

import com.badlogic.gdx.Gdx;

public class Vars {
	
	//pixels per meters
	public static float PPM = 100;
	
	//virtual game world size
	public static float WIDTH = 8f;
	public static float HEIGHT = 4.8f;
	
	//virtual game player size
	public static float PLAYER_WIDTH = 1f;
	public static float PLAYER_HEIGHT = 0.45f;
	
	//virtual game player size
	public static float PLAYER_INIT_X = 0;
	public static float PLAYER_INIT_Y = 0;
	
	//virtual game level size
	public static float LEVEL_WIDTH = 128f;
	public static float LEVEL_HEIGHT = HEIGHT;
	
	//virtual game borders size
	public static float VERTICAL_BORDER_WIDTH = 0.01f;
	public static float VERTICAL_BORDER_HEIGH = HEIGHT;
	public static float HORIZONTAL_BORDER_WIDTH = LEVEL_WIDTH;
	public static float HORIZONTAL_BORDER_HEIGH = 0.01f;
	
	//virtual game borders coordinates
	public static float LEFT_BORDER_X = -WIDTH/2;
	public static float LEFT_BORDER_Y = 0;
	public static float TOP_BORDER_X = LEVEL_WIDTH/2-WIDTH/2;
	public static float TOP_BORDER_Y = HEIGHT/2;
	public static float RIGHT_BORDER_X = LEVEL_WIDTH-WIDTH/2;
	public static float RIGHT_BORDER_Y = 0;
	public static float BOTTOM_BORDER_X = LEVEL_WIDTH/2-WIDTH/2;
	public static float BOTTOM_BORDER_Y = -HEIGHT/2;
	
	//real game size
	public static float REAL_WIDTH;
	public static float REAL_HEIGHT;
	
	//scale virtual values to real
	public static void init(float width, float height){
		REAL_WIDTH = width/PPM;
		REAL_HEIGHT = height/PPM;
		
		VERTICAL_BORDER_WIDTH = scale(0.01f);
		VERTICAL_BORDER_HEIGH = scale(HEIGHT);
		HORIZONTAL_BORDER_WIDTH = scale(LEVEL_WIDTH);
		HORIZONTAL_BORDER_HEIGH = scale(0.01f);
		
		LEFT_BORDER_X = scale(-WIDTH/2);
		LEFT_BORDER_Y = scale(0);
		TOP_BORDER_X = scale(LEVEL_WIDTH/2-WIDTH/2);
		TOP_BORDER_Y = scale(HEIGHT/2);
		RIGHT_BORDER_X = scale(LEVEL_WIDTH-WIDTH/2);
		RIGHT_BORDER_Y = scale(0);
		BOTTOM_BORDER_X = scale(LEVEL_WIDTH/2-WIDTH/2);
		BOTTOM_BORDER_Y = scale(-HEIGHT/2);
	}
	
	//coef to scale
	public static float scaleCoef(){
		return REAL_HEIGHT/HEIGHT;
	}
	
	//scale method
	public static float scale(float value){
		return value*scaleCoef();
	}
	
	//properties
	public static final float DEFAULT_DENSITY = 10;
	public static final float PLANE_DENSITY = 2;
	
	//player shape size
	public static float PLAYER_SHAPE_WIDTH = 400;
	public static float PLAYER_SHAPE_HEIGHT = 180;
	
	//scale shape to virtual game size
	public static float PLAYER_SHAPE_WIDTH_SCALE = PLAYER_WIDTH/PLAYER_SHAPE_WIDTH;
	public static float PLAYER_SHAPE_HEIGHT_SCALE = PLAYER_HEIGHT/PLAYER_SHAPE_HEIGHT;
	
	//player shape
	public static float[] PLAYER_SHAPE_VERTICES = {
			5,40,
			5,80,
			15,90,
			40,95,
			240,120,
			300,120,
			310,150,
			340,150,
			360,120,
			380,130,
			380,100,
			390,100,
			390,180,
			400,100,
			390,10,
			390,90,
			380,90,
			380,60,
			330,60,
			340,30,
			350,30,
			350,15,
			260,15,
			260,30,
			290,30,
			280,60,
			60,65,
			60,40,
			50,30,
			15,30,
			15,40,
			5,40
	};
	//levels vars
	public static final String DEFAULT_PATH_TO_LEVELS = Gdx.files.getLocalStoragePath();
	public static final String DEFAULT_PATH_TO_OBJECT_MAP = Gdx.files.getLocalStoragePath();
	


}
