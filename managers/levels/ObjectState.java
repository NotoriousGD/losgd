package com.losgd.los.managers.levels;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public class ObjectState implements Json.Serializable {

	int id;
	float x, y, width, height;

	public ObjectState() {
	};

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void write(Json json) {
		json.writeValue("id", getId());
		json.writeValue("x", getX());
		json.writeValue("y", getY());
		json.writeValue("width", getWidth());
		json.writeValue("height", getHeight());
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		setId(jsonData.get("id").asInt());
		setX(jsonData.get("x").asFloat());
		setY(jsonData.get("y").asFloat());
		setWidth(jsonData.get("width").asFloat());
		setHeight(jsonData.get("height").asFloat());
	}
}
