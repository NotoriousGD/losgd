package com.losgd.los.managers.levels;

import com.badlogic.gdx.utils.Array;

public class Level {

    private int id;
    private Array<ObjectState> objects;

    public Level() {

        id = 0;
        objects = new Array<ObjectState>();

    }

    public Array<ObjectState> getObjects() {
        return objects;
    }

    public void setObjects(Array<ObjectState> objects) {
        this.objects = objects;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}