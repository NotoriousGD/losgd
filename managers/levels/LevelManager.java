package com.losgd.los.managers.levels;

import static com.losgd.los.Vars.DEFAULT_PATH_TO_LEVELS;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.losgd.los.managers.ObjectManager;
import com.losgd.los.models.GameObject;
import com.losgd.los.models.GameWorld;

public class LevelManager {

	private int levelId;

	public static Array<GameObject> objects;

	private static LevelManager instance;

	public static LevelManager getInstance() {
		if (null == instance) {
			instance = new LevelManager();
		}
		return instance;
	}

	private LevelManager() {

		objects = new Array<GameObject>();

	}

	public void init(GameWorld gameWorld, int levelId) {
		Level level = readFile(levelId);
		ObjectManager.init();
		for (ObjectState objectState : level.getObjects()) {
			ObjectManager.addObjectToLevel(objectState, gameWorld);
		}
	}

	public Level readFile(int levelId) {
		FileHandle file = Gdx.files.absolute(DEFAULT_PATH_TO_LEVELS + levelId);
		Json json = new Json();
		String data = file.readString();
		return json.fromJson(Level.class, data);
	}

	public void writeFile(Level level) {
		FileHandle file = Gdx.files.absolute(DEFAULT_PATH_TO_LEVELS + level.getId());
		Json json = new Json();
		System.out.println("save level " + level.getId() + ":");
		System.out.println(json.prettyPrint(level));
		System.out.println("to file:" + file.pathWithoutExtension());
		file.writeString(json.toJson(level), false);
	}

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public static Array<GameObject> getObjects() {
		return objects;
	}

}
