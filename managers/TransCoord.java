package com.losgd.los.managers;

import static com.losgd.los.Vars.scale;

public class TransCoord {

	private static TransCoord instance;

	public static TransCoord getInstance() {
		if (null == instance) {
			instance = new TransCoord();
		}
		return instance;
	}

	private static float[] verticesOut;

	// start angle system coord
	public enum StartingPoint {
		LEFT_UP(1, -1, -1, 1), LEFT_DOWN(1, 1, -1, -1), RIGHT_UP(-1, -1, 1, 1), RIGHT_DOWN(-1, 1, 1, -1);

		private int[] value;

		private StartingPoint(int x, int y, int cx, int cy) {
			value = new int[4];
			value[0] = x;
			value[1] = y;
			value[2] = cx;
			value[3] = cy;
		}

		public int[] getValue() {
			return value;
		}
	}

	// start point
	private static StartingPoint startingPoint = StartingPoint.LEFT_UP;

	// central
	private static float centerX, centerY;

	public static float[] translate(float[] in, float x, float y) {
		centerX = x;
		centerY = y;
		verticesOut = new float[in.length];
		boolean selector = true;
		int index = 0;
		for (float vertex : in) {
			if (selector) {
				verticesOut[index] = scale(startingPoint.value[2] * centerX + startingPoint.value[0] * vertex);
				index++;
				selector = false;
				continue;
			}
			verticesOut[index] = scale(startingPoint.value[3] * centerY + startingPoint.value[1] * vertex);
			index++;
			selector = true;
		}
		return verticesOut;
	}

	// use scale
	public static float[] translate(float[] in, float x, float y, float scaleX, float scaleY) {
		centerX = x;
		centerY = y;
		verticesOut = new float[in.length];
		boolean selector = true;
		int index = 0;
		for (float vertex : in) {
			if (selector) {
				verticesOut[index] = scale(startingPoint.value[2] * centerX + startingPoint.value[0] * vertex) * scaleX;
				index++;
				selector = false;
				continue;
			}
			verticesOut[index] = scale(startingPoint.value[3] * centerY + startingPoint.value[1] * vertex) * scaleY;
			index++;
			selector = true;
		}
		return verticesOut;
	}

	public static StartingPoint getStartingPoint() {
		return startingPoint;
	}

	public static void setStartingPoint(StartingPoint startingPoint) {
		TransCoord.startingPoint = startingPoint;
	}

	public static float getCenterX() {
		return centerX;
	}

	public static void setCenterX(float centerX) {
		TransCoord.centerX = centerX;
	}

	public static float getCenterY() {
		return centerY;
	}

	public static void setCenterY(float centerY) {
		TransCoord.centerY = centerY;
	}
}
