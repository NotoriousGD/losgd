package com.losgd.los.managers;

import com.badlogic.gdx.assets.AssetManager;

public class GameAssetManager extends AssetManager {

	private static GameAssetManager instance;

	private GameAssetManager() {

	}

	public static GameAssetManager getInstance() {
		if (null == instance) {
			instance = new GameAssetManager();
		}
		return instance;
	}
}
