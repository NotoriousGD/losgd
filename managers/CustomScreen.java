package com.losgd.los.managers;

import com.losgd.los.screens.BattleScreen;
import com.losgd.los.screens.GameScreen;
import com.losgd.los.screens.LoadScreen;
import com.losgd.los.screens.MenuScreen;

public enum CustomScreen {
	LOAD_SCREEN {
		@Override
		protected com.badlogic.gdx.Screen getScreenInstance() {
			return new LoadScreen();
		}
	},

	MENU_SCREEN {
		@Override
		protected com.badlogic.gdx.Screen getScreenInstance() {
			return new MenuScreen();
		}
	},

	GAME_SCREEN {
		@Override
		protected com.badlogic.gdx.Screen getScreenInstance() {
			return new GameScreen();
		}
	},

	BATTLE_SCREEN {
		@Override
		protected com.badlogic.gdx.Screen getScreenInstance() {
			return new BattleScreen();
		}
	};

	protected abstract com.badlogic.gdx.Screen getScreenInstance();
}
