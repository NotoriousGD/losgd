package com.losgd.los.managers;

import static com.losgd.los.Vars.BOTTOM_BORDER_X;
import static com.losgd.los.Vars.BOTTOM_BORDER_Y;
import static com.losgd.los.Vars.HORIZONTAL_BORDER_HEIGH;
import static com.losgd.los.Vars.HORIZONTAL_BORDER_WIDTH;
import static com.losgd.los.Vars.LEFT_BORDER_X;
import static com.losgd.los.Vars.LEFT_BORDER_Y;
import static com.losgd.los.Vars.RIGHT_BORDER_X;
import static com.losgd.los.Vars.RIGHT_BORDER_Y;
import static com.losgd.los.Vars.TOP_BORDER_X;
import static com.losgd.los.Vars.TOP_BORDER_Y;
import static com.losgd.los.Vars.VERTICAL_BORDER_HEIGH;
import static com.losgd.los.Vars.VERTICAL_BORDER_WIDTH;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.IntMap;
import com.losgd.los.managers.levels.LevelManager;
import com.losgd.los.managers.levels.ObjectState;
import com.losgd.los.models.GameObject;
import com.losgd.los.models.GameWorld;
import com.losgd.los.models.Plane;
import com.losgd.los.models.Platform;

public class ObjectManager {

	public static IntMap<GameObject> gameObjects = new IntMap<GameObject>();

	public static void addObjectToLevel(ObjectState objectState, GameWorld gameWorld) {
		GameObject object = gameObjects.get(objectState.getId());
		object.init(gameWorld, objectState.getX(), objectState.getY(), objectState.getWidth(), objectState.getHeight());
		LevelManager.objects.add(object);
	}

	public static void init() {
		gameObjects.put(0, new Plane());
		gameObjects.put(1, new Platform());
	}
	
	//simple create a body
	public static Body createObject(World world, float x, float y, float width, float height, BodyType type) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = type;
		bodyDef.position.set(x, y);
		Body body = world.createBody(bodyDef);
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(width / 2, height / 2);
		body.createFixture(shape, 1);
		shape.dispose();
		return body;
	}

	//simple create a base form for level
	public static void createLevel(World world, float width, float height) {
		createObject(world, LEFT_BORDER_X, LEFT_BORDER_Y, VERTICAL_BORDER_WIDTH, VERTICAL_BORDER_HEIGH,
				BodyType.StaticBody);
		createObject(world, TOP_BORDER_X, TOP_BORDER_Y, HORIZONTAL_BORDER_WIDTH, HORIZONTAL_BORDER_HEIGH,
				BodyType.StaticBody);
		createObject(world, RIGHT_BORDER_X, RIGHT_BORDER_Y, VERTICAL_BORDER_WIDTH, VERTICAL_BORDER_HEIGH,
				BodyType.StaticBody);
		createObject(world, BOTTOM_BORDER_X, BOTTOM_BORDER_Y, HORIZONTAL_BORDER_WIDTH, HORIZONTAL_BORDER_HEIGH,
				BodyType.StaticBody);
	}

}
