package com.losgd.los.managers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.losgd.los.models.Plane;

public class GameInformationManager {

	private GameInformationManager() {

	}

	private static BitmapFont bitmapFont = new BitmapFont();
	private static GameInformationManager instance;

	public static GameInformationManager getInstance() {
		if (null == instance) {
			instance = new GameInformationManager();
		}
		return instance;
	}

	public static void displayInformation(SpriteBatch spriteBatch, Plane player,
			OrthographicCamera orthographicCamera) {
		bitmapFont.getClass();
	}
}
