package com.losgd.los;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.losgd.los.managers.CustomScreen;
import com.losgd.los.managers.ScreenManager;

public class LoS extends Game {
	@Override
	public void create() {
		Vars.init(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		ScreenManager.getInstance().init(this);
		ScreenManager.getInstance().show(CustomScreen.LOAD_SCREEN);
	}
}
