package com.losgd.los.controllers;

import static com.losgd.los.Vars.PLAYER_INIT_X;
import static com.losgd.los.Vars.PLAYER_INIT_Y;
import static com.losgd.los.Vars.scale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.losgd.los.models.Plane;

public class Controller implements InputProcessor {
	
	public float timer;

	public Plane plane;
	public OrthographicCamera orthographicCamera;

	public Controller(Plane plane, OrthographicCamera orthographicCamera) {
		this.plane = plane;
		this.orthographicCamera = orthographicCamera;
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {

		if (amount > 0) {
			orthographicCamera.zoom++;
		} else {
			orthographicCamera.zoom--;
		}
		return false;
	}

	float lastX, lastY;
	float offsetY = 100f;

	public void step(float delta) {
		
		timer+=delta;
		
		orthographicCamera.translate(plane.body.getPosition().x - lastX, plane.body.getPosition().y - lastY, 0);
		
		lastX = plane.body.getPosition().x;
		lastY = plane.body.getPosition().y;
		
		if(timer > 0.2){
			//for example, another will complete later
			plane.body.applyForceToCenter(scale(10), 0, true);
			timer = 0;
		}
		
		if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
			plane.body.setTransform(scale(PLAYER_INIT_X), scale(PLAYER_INIT_Y), 0);
		}
		plane.update();
	}
}
