package com.losgd.los.models;

import static com.losgd.los.Vars.PLAYER_SHAPE_VERTICES;
import static com.losgd.los.Vars.scale;
import static com.losgd.los.Vars.PLAYER_SHAPE_WIDTH_SCALE;
import static com.losgd.los.Vars.PLAYER_SHAPE_HEIGHT_SCALE;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Array;
import com.losgd.los.managers.TransCoord;
import com.losgd.los.views.AnimationConstructor;

public class Plane extends GameObject {

	private Array<AnimationConstructor> anim;

	@Override
	public void init(GameWorld gameWorld, float x, float y, float width, float height) {

		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(scale(x), scale(y));

		body = gameWorld.getWorld().createBody(bodyDef);

		this.body.setBullet(true);
		addChainFixture(TransCoord.translate(PLAYER_SHAPE_VERTICES, 200, 100, PLAYER_SHAPE_WIDTH_SCALE, PLAYER_SHAPE_HEIGHT_SCALE));

		// anim = new Array<AnimationConstructor>();
		// anim.addAll(AnimationPacker.animations.get("Plane_main"));
	}

	@Override
	public void update() {
	}

	@Override
	public void render(SpriteBatch batch) {
		for (AnimationConstructor rendAnim : anim) {
			rendAnim.render(body, batch);
		}
	}

	@Override
	public void setState() {

	}

}
