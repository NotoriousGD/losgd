package com.losgd.los.models;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.losgd.los.views.GameRenderer;

public class Platform extends GameObject {

	public float width, height;

	public Platform() {
	}

	@Override
	public void init(GameWorld gameWorld, float x, float y, float width, float height) {

		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(x, y);

		body = gameWorld.getWorld().createBody(bodyDef);

		addPolyFixture((int) width, (int) height, 0, 0, 0);
		this.width = width;
		this.height = height;
	}

	@Override
	public void setState() {
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(animation.getKeyFrame(GameRenderer.maintime, true), body.getPosition().x - width / 2,
				body.getPosition().y - height / 2, width / 2, height / 2, width, height, 1, 1,
				body.getAngle() * MathUtils.radiansToDegrees);
	}

	@Override
	public void update() {

	}
}
