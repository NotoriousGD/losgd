package com.losgd.los.models;

import static com.losgd.los.Vars.LEVEL_HEIGHT;
import static com.losgd.los.Vars.LEVEL_WIDTH;
import static com.losgd.los.Vars.PLAYER_HEIGHT;
import static com.losgd.los.Vars.PLAYER_INIT_X;
import static com.losgd.los.Vars.PLAYER_INIT_Y;
import static com.losgd.los.Vars.PLAYER_WIDTH;
import static com.losgd.los.Vars.REAL_HEIGHT;
import static com.losgd.los.Vars.REAL_WIDTH;
import static com.losgd.los.Vars.scale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.losgd.los.managers.ObjectManager;
import com.losgd.los.views.GameRenderer;

public class GameWorld {

	private World world;
	private Plane player;
	public GameRenderer renderer;
	private OrthographicCamera camera;

	public GameWorld() {
		world = new World(new Vector2(0, 0), true);
		player = new Plane();
		player.init(this, scale(PLAYER_INIT_X), scale(PLAYER_INIT_Y), scale(PLAYER_WIDTH), scale(PLAYER_HEIGHT));
		camera = new OrthographicCamera();
		camera.setToOrtho(false, REAL_WIDTH, REAL_HEIGHT);
		camera.position.set(player.body.getPosition().x, player.body.getPosition().y, 0);
		camera.update();

		// it's need replace to level but i was lazy
		// level base form
		ObjectManager.createLevel(world, LEVEL_WIDTH, LEVEL_HEIGHT);
		/*
		 * renderer = new GameRenderer();
		 * renderer.getCamera(orthographicCamera); renderer.LoadPlayer(player);
		 */
	}

	public void step() {
		world.step(Gdx.graphics.getDeltaTime(), 6, 3);
		camera.update();
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// renderer.render();
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public Plane getPlayer() {
		return player;
	}

	public void setPlayer(Plane player) {
		this.player = player;
	}

	public OrthographicCamera getOrthographicCamera() {
		return camera;
	}

	public void setOrthographicCamera(OrthographicCamera orthographicCamera) {
		this.camera = orthographicCamera;
	}

}
