package com.losgd.los.models;

import static com.losgd.los.Vars.DEFAULT_DENSITY;
import static com.losgd.los.Vars.PLANE_DENSITY;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public abstract class GameObject {

	public Body body;
	public Animation animation;

	public GameObject() {
	}

	public abstract void init(GameWorld gameWorld, float x, float y, float width, float height);

	public void addPolyFixture(int width, int height, float x, float y, float angle) {

		PolygonShape polygonShape = new PolygonShape();
		polygonShape.setAsBox(width, height, new Vector2(x, y), angle);

		body.createFixture(polygonShape, DEFAULT_DENSITY);

		polygonShape.dispose();
	}

	public void addChainFixture(float[] vertices) {

		ChainShape chainShape = new ChainShape();
		chainShape.createChain(vertices);

		body.createFixture(chainShape, PLANE_DENSITY);

		chainShape.dispose();
	}

	// protected void loadAnimation(int id){
	// animation = GameRenderer.getAnimation(id);
	// }

	public abstract void update();

	public abstract void setState();

	public abstract void render(SpriteBatch batch);
}
